CC=g++
EXECUTABLE=main
OBJECT_DIR=object
ICNLUDE_DIR=include
SOURCE_DIR=source

ifeq ($(BUILD),release)
CFLAGS=-O3 -std=c++17 -pthread -Wall -Wextra -Wshadow -Wold-style-cast -Wpedantic\
-Wfloat-equal -Wpointer-arith -Wcast-qual -Wstrict-overflow=5 -Wwrite-strings\
-Wswitch-default -Wswitch-enum -Wunreachable-code -Winit-self\
-Wconversion
-Wno-unused-parameter
else
CFLAGS=-g3 -O0 -std=c++17 -pthread -Wall -Wextra -Wshadow -Wold-style-cast -Wpedantic\
-Wfloat-equal -Wpointer-arith -Wcast-qual -Wstrict-overflow=5 -Wwrite-strings\
-Wswitch-default -Wswitch-enum -Wunreachable-code -Winit-self\
-Wconversion
#-Wno-unused-parameter
endif

INCLUDE_FLAG=-I$(ICNLUDE_DIR)
LIBS=

# Find all source files
SOURCE_NAMES=$(shell cd $(SOURCE_DIR); find -name '*.cpp' -printf '%P\n' )
# Get all object files by substituting .cpp with .o
OBJECT_NAMES=$(SOURCE_NAMES:%.cpp=%.o)
OBJECT_PATHS=$(patsubst %,$(OBJECT_DIR)/%,$(OBJECT_NAMES))

.PHONY: clean release

$(EXECUTABLE): $(OBJECT_PATHS)
	$(CC) -o $@ $^ $(CFLAGS) $(INCLUDE_FLAG) $(LIBS)

$(OBJECT_DIR)/main.o: $(SOURCE_DIR)/main.cpp
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDE_FLAG)

$(OBJECT_DIR)/%.o: $(SOURCE_DIR)/%.cpp $(ICNLUDE_DIR)/%.h
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDE_FLAG)


release:
	make clean
	make -j 4 "BUILD=release"

clean:
	rm -f $(OBJECT_DIR)/*.o
	rm -f $(EXECUTABLE)
