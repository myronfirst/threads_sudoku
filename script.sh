#!/bin/bash

#colors
RESET='\e[0m';
GREEN_BOLD='\e[1;32m';
RED_BOLD='\e[1;31m';
YELLOW_BOLD='\e[1;33m';

pass_count=0;
fail_count=0;
for i in `ls test/*.in`; do
    f=$(basename -s .in $i)

    echo -e $i'\t:\t'$YELLOW_BOLD'RUNNING'$RESET;
    # run the executable to produce the result
    ./main 'test/'$f'.in' 'test/'$f'.out'
    ans=$?
    #valgrind --leak-check=full ./main $i

    # Diff
    # ignoring blank lines (-B)
    # ignoring trailing white space at line end (-Z)
    # ignoring changes in amount of white space (-b)
    # stripping trailing cr \r
    #diff -B -Z -b --strip-trailing-cr out.txt test/${f%in}out;
    #ans=$?

    # Print the test filename
    echo -e -n $i'\t:\t';
    # Check if there where any diffs or not
    if [ $ans -eq 0 ]; then
        echo -e $GREEN_BOLD'PASS'$RESET;
        ((++pass_count))
    else
        echo -e $RED_BOLD'FAIL'$RESET;
        ((++fail_count))
    fi

    # remove results
    rm -f out.txt
done
total_count=$(($pass_count + $fail_count));
echo -n $pass_count' / '$total_count;
echo -e '\t'$GREEN_BOLD'PASSED'$RESET;
