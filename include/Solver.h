#ifndef _SOLVER_H_
#define _SOLVER_H_

#include <functional>
#include <mutex>
#include <optional>
#include <stack>
#include <string>

#include "Grid.h"

class Solver {
  public:
    using Index = Grid::Index;
    using Value = Grid::Value;
    using GridVec = Grid::GridVec;
    using Algorithm = std::function<bool(Grid&)>;
    enum class FINDCANDIDATE_MSG {
        FAILED = 0,
        SOLVED = 1,
        SINGLE,
        MULTIPLE,
        FINDCANDIDATE_MSG_MAX
    };
    enum class APPLYHEURISTICS_MSG {
        FAILED = 0,
        SOLVED = 1,
        BACKTRACK,
        DOHEURISTICS_MSG_MAX
    };
    enum class ALGORITHM_LABEL {
        NAIVE,
        CANDIDATE,
        THREAD_NAIVE,
        THREAD_CANDIDATE,
        ALGORITHM_MAX
    };

  private:
    Grid input;
    std::vector<Grid> solutions{};
    bool stopSolving{true};
    Algorithm algorithm{};
    Algorithm threadAlgorithm{};
    std::mutex emplaceMutex{};
    std::mutex popMutex{};

    auto FindCandidate(const Grid& grid) -> std::tuple<Index, Value, FINDCANDIDATE_MSG>;
    auto ApplyHeuristics(Grid& grid) -> std::tuple<Index, APPLYHEURISTICS_MSG>;
    bool Backtrack(Grid& grid, Index minIndex);
    bool CandidateBacktrackLinearSolveImpl(Grid& grid);
    bool NaiveBacktrackLinearSolveImpl(Grid& grid, Grid::Index index);
    void ThreadLinearSolveImpl(Grid& grid);

    auto GenerateThreadInputGrids(const Grid& grid, size_t size) -> std::stack<Grid>;
    void SpawnThreads(std::stack<Grid>& threadInputGrids, size_t size);
    void ThreadSolve(std::stack<Grid>& threadInputGrids);

    bool StopSolving(const Grid& grid);

    void EmplaceSafe(std::vector<Grid>& vec, const Grid& val);
    auto PopSafe(std::stack<Grid>& stack) -> std::optional<Grid>;

  public:
    Solver(const Grid& _input, ALGORITHM_LABEL algorithmLabel, bool _stopSolving = true);
    ~Solver() = default;
    Solver() = delete;
    Solver(const Solver& other) = delete;
    Solver(Solver&& other) = delete;
    Solver& operator=(const Solver& other) = delete;
    Solver& operator=(Grid&& other) = delete;

    bool CandidateBacktrackLinearSolve(Grid& grid);
    bool NaiveBacktrackLinearSolve(Grid& grid);
    bool ThreadLinearSolve(Grid& grid);
    auto Solve() -> std::vector<GridVec>;

    void AssertSolutions() const;
    void PrintSolutions(const std::string& filename = "") const;
};

#endif
