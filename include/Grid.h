#ifndef _GRID_H_
#define _GRID_H_

#include <bitset>
#include <cstdint>
#include <string>
#include <vector>

struct Grid {
    using Index = uint32_t;
    using Value = uint8_t;
    using RowCol = std::pair<Index, Index>;

    static constexpr Value CAND_CAP = (128 + 1);
    using Candidates = std::bitset<CAND_CAP>;
    using GridVec = std::vector<Value>;
    using CandidatesVec = std::vector<Candidates>;

    /* fields */
    GridVec grid{};
    // std::vector<GridVec> solutions{};
    CandidatesVec gridCandidates{};
    Index size{0}, sizeDim{0}, sizeTotal{0}; /* size: box row/col, sizeDim: grid row/col, sizeTotal: total cells */
    Index emptyCells{0};

    /* constructors/destructors/operators */
    Grid(const std::string& fileName);
    Grid(const Grid& other) = default;
    Grid(Grid&& other) = default;
    ~Grid() = default;
    Grid& operator=(const Grid& other) = default;
    Grid& operator=(Grid&& other) = default;
    Grid() = delete;

    /* candidate methods */
    auto GetCandidates(Index index) const -> const Candidates&;
    auto GetCandidates(const RowCol& rowcol) const -> const Candidates& { return GetCandidates(GetIndex(rowcol)); }
    auto GetCandidatesNoConst(Index index) -> Candidates& { return const_cast<Candidates&>(GetCandidates(index)); }
    auto GetCandidatesNoConst(const RowCol& rowcol) -> Candidates& { return const_cast<Candidates&>(GetCandidates(rowcol)); }

    auto GenerateCandidates(const RowCol& rowcol) -> const Candidates&;
    auto GenerateCandidates(Index index) -> const Candidates& { return GenerateCandidates(GetRowCol(index)); }

    void GenerateGridCandidates();

    bool IsValidEliminationState(Index index, Value value) const;
    void Eliminate(Index index, Value value);
    void MinusEmptyCells() { --emptyCells; }
    bool EliminateAndPut(Index index, Value value);

    auto GetEmptyCells() const -> std::vector<Index>;
    auto GetValidCandidates(Index index) const -> std::vector<Value>;

    /* primitive methods */
    constexpr Index GetIndex(const RowCol& rowcol) const { return rowcol.first * sizeDim + rowcol.second; }
    constexpr RowCol GetRowCol(Index index) const { return {index / sizeDim, index % sizeDim}; }
    Value Get(Index index) const { return grid.at(index); }
    Value Get(const RowCol& rowcol) const { return grid.at(GetIndex(rowcol)); }
    void Put(Index index, Value value) { grid.at(index) = value; }
    void Put(const RowCol& rowcol, Value value) { grid.at(GetIndex(rowcol)) = value; }

    /* sanity methods */
    bool IsComplete() const;
    bool IsValid() const;

    /* print methods */
    std::string ToString() const;
    void Print(const std::string& filename = "") const;
    void PrintAllCandidates() const;

    /* unused */
    void RestoreEliminate(Index index, Value value);
};

#endif
