#ifndef _VALIDATOR_H_
#define _VALIDATOR_H_

#include <string>
#include <vector>

#include "Grid.h"

class Validator {
  public:
    using Index = Grid::Index;
    using Value = Grid::Value;
    using GridVec = std::vector<Value>;

  private:
    std::vector<GridVec> inputSolutions{};
    std::vector<GridVec> validSolutions{};

  public:
    Validator(const std::vector<GridVec>& _inputSolutions, const std::string& fileName);
    ~Validator() = default;
    Validator() = delete;
    Validator(const Validator& other) = delete;
    Validator(Validator&& other) = delete;
    Validator& operator=(const Validator& other) = delete;
    Validator& operator=(Validator&& other) = delete;

    bool Validate();
};
#endif
