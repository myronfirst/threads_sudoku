#include "Grid.h"

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>

// static std::chrono::time_point timer0;
// static std::chrono::time_point timer1;

/* constructors/destructors/operators */
Grid::Grid(const std::string& fileName) {
    std::ifstream ifs(fileName);
    if (!ifs.is_open()) {
        std::cout << "Check file path" << std::endl;
        exit(EXIT_FAILURE);
    }
    unsigned long long num;
    ifs >> num;
    while (ifs) {
        grid.emplace_back(static_cast<Value>(num));
        gridCandidates.emplace_back();
        if (num == 0) ++emptyCells;
        ifs >> num;
    }
    grid.shrink_to_fit();

    sizeTotal = static_cast<Index>(grid.size());
    sizeDim = static_cast<Index>(std::sqrt(sizeTotal));
    size = static_cast<Index>(std::sqrt(sizeDim));
    GenerateGridCandidates();
}

/* candidate methods */
auto Grid::GetCandidates(Index index) const -> const Candidates& {
    assert(Get(index) == 0);
    return gridCandidates.at(index);
}

auto Grid::GenerateCandidates(const RowCol& rowcol) -> const Candidates& {
    const auto& [row, col] = rowcol;
    auto ones = std::bitset<CAND_CAP>((1 << (sizeDim + 1)) - 1);    //least-most sizeDim candidates are ones
    ones.reset(0);
    auto& cands = GetCandidatesNoConst(rowcol);
    cands |= ones;             //set least-most sizeDim candidates to ones
    assert(!cands.test(0));    //not bit 0
    //col
    for (Index i = 0; i < sizeDim; ++i) {
        Value val = Get({i, col});
        if (val == 0) continue;
        cands.reset(val);
    }
    //row
    for (Index i = 0; i < sizeDim; ++i) {
        Value val = Get({row, i});
        if (val == 0) continue;
        cands.reset(val);
    }
    //box
    Index boxRStart = (row / size) * size;
    Index boxCStart = (col / size) * size;
    for (Index i = boxRStart; i < boxRStart + size; ++i) {
        for (Index j = boxCStart; j < boxCStart + size; ++j) {
            Value val = Get({i, j});
            if (val == 0) continue;
            cands.reset(val);
        }
    }
    return cands;
}

void Grid::GenerateGridCandidates() {
    auto ones = std::bitset<CAND_CAP>((1 << (sizeDim + 1)) - 1);    //least-most sizeDim candidates are ones
    ones.reset(0);                                                  //not bit 0
    for (Index index = 0; index < sizeTotal; ++index) {
        Value value = Get(index);
        if (value > 0) continue;
        const auto& [row, col] = GetRowCol(index);
        auto& cands = GetCandidatesNoConst({row, col});
        cands |= ones;             //set least-most sizeDim candidates to ones
        assert(!cands.test(0));    //not bit 0
        //col
        for (Index i = 0; i < sizeDim; ++i) {
            Value val = Get({i, col});
            if (val == 0) continue;
            cands.reset(val);
        }
        //row
        for (Index i = 0; i < sizeDim; ++i) {
            Value val = Get({row, i});
            if (val == 0) continue;
            cands.reset(val);
        }
        //box
        Index boxRStart = (row / size) * size;
        Index boxCStart = (col / size) * size;
        for (Index i = boxRStart; i < boxRStart + size; ++i) {
            for (Index j = boxCStart; j < boxCStart + size; ++j) {
                Value val = Get({i, j});
                if (val == 0) continue;
                cands.reset(val);
            }
        }
    }
}

bool Grid::IsValidEliminationState(Index index, Value value) const {
    const auto& [row, col] = GetRowCol(index);
    assert(value > 0);

    for (Index i = 0; i < sizeDim; ++i) {
        if (i == row || Get({i, col}) > 0) continue;
        auto& cands = GetCandidates({i, col});
        if (cands.test(value)) assert(cands.count() >= 1);
        if (cands.test(value) && cands.count() - 1 == 0) return false;    //invalid state: a cell other than @index, has @value as single candidate
    }
    for (Index i = 0; i < sizeDim; ++i) {
        if (i == col || Get({row, i}) > 0) continue;
        auto& cands = GetCandidates({row, i});
        if (cands.test(value)) assert(cands.count() >= 1);
        if (cands.test(value) && cands.count() - 1 == 0) return false;    //invalid state: a cell other than @index, has @value as single candidate
    }
    Index boxRStart = (row / size) * size;
    Index boxCStart = (col / size) * size;
    for (Index i = boxRStart; i < boxRStart + size; ++i) {
        for (Index j = boxCStart; j < boxCStart + size; ++j) {
            if ((i == row && j == col) || Get({i, j}) > 0) continue;
            auto& cands = GetCandidates({i, j});
            if (cands.test(value)) assert(cands.count() >= 1);
            if (cands.test(value) && cands.count() - 1 == 0) return false;    //invalid state: a cell other than @index, has @value as single candidate
        }
    }
    return true;
}

void Grid::Eliminate(Index index, Value value) {
    const auto& [row, col] = GetRowCol(index);
    assert(value > 0);

    for (Index i = 0; i < sizeDim; ++i) {
        if (Get({i, col}) > 0) continue;
        auto& cands = GetCandidatesNoConst({i, col});
        cands.reset(value);
    }
    for (Index i = 0; i < sizeDim; ++i) {
        if (Get({row, i}) > 0) continue;
        auto& cands = GetCandidatesNoConst({row, i});
        cands.reset(value);
    }
    Index boxRStart = (row / size) * size;
    Index boxCStart = (col / size) * size;
    for (Index i = boxRStart; i < boxRStart + size; ++i) {
        for (Index j = boxCStart; j < boxCStart + size; ++j) {
            if (Get({i, j}) > 0) continue;
            auto& cands = GetCandidatesNoConst({i, j});
            cands.reset(value);
        }
    }
}

bool Grid::EliminateAndPut(Index index, Value value) {
    if (IsValidEliminationState(index, value) == false) return false;    //found cell with invalid state after IsValidEliminationState
    Eliminate(index, value);
    Put(index, value);
    MinusEmptyCells();
    return true;
}

auto Grid::GetEmptyCells() const -> std::vector<Index> {
    std::vector<Index> vec{};
    for (Index i = 0; i < sizeTotal; ++i)
        if (Get(i) == 0) vec.emplace_back(i);
    return vec;
}
auto Grid::GetValidCandidates(Index index) const -> std::vector<Value> {
    std::vector<Value> vec{};
    auto& cands = GetCandidates(index);
    for (Value val = 1; val <= sizeDim; ++val)
        if (cands.test(val)) vec.emplace_back(val);
    return vec;
}

/* sanity methods */
bool Grid::IsComplete() const {
    for (Index i = 0; i < sizeTotal; ++i)
        if (Get(i) == 0) return false;
    return true;
}

bool Grid::IsValid() const {
    std::bitset<CAND_CAP> set(false);    //set of seen value
    //rows
    for (Index i = 0; i < sizeDim; ++i) {
        set.reset();    //initialize set of seens to false
        for (Index j = 0; j < sizeDim; ++j) {
            Value val = Get({i, j});
            assert(val <= sizeDim);             //cell has valid value [0, sizeDim]
            if (val == 0) continue;             //cell is empty
            if (set.test(val)) return false;    //value already seen, invalid grid
            set.set(val);                       //value seen first time
        }
    }
    //cols
    for (Index i = 0; i < sizeDim; ++i) {
        set.reset();
        for (Index j = 0; j < sizeDim; ++j) {
            Value val = Get({j, i});
            assert(val <= sizeDim);
            if (val == 0) continue;
            if (set.test(val)) return false;
            set.set(val);
        }
    }
    //boxes
    for (Index index = 0; index < sizeTotal; index += sizeDim) {
        set.reset();
        Index row = index / sizeDim;
        Index col = index % sizeDim;
        Index boxRStart = (row / size) * size;
        Index boxCStart = (col / size) * size;
        for (Index i = boxRStart; i < boxRStart + size; ++i) {
            for (Index j = boxCStart; j < boxCStart + size; ++j) {
                Value val = Get({j, i});
                assert(val <= sizeDim);
                if (val == 0) continue;
                if (set.test(val)) return false;
                set.set(val);
            }
        }
    }
    return true;
}

/* print methods */
std::string Grid::ToString() const {
    std::ostringstream oss{};
    for (Index i = 0; i < sizeDim; ++i) {
        for (Index j = 0; j < sizeDim; ++j) {
            oss << static_cast<unsigned long long>(Get({i, j})) << " ";
        }
        oss << std::endl;
    }
    return oss.str();
}

void Grid::Print(const std::string& filename) const {
    std::ofstream ofs;
    if (!filename.empty()) {
        ofs.open(filename, std::ios::out);
    }
    std::ostream& os = (ofs.is_open() ? ofs : std::cout);
    os << ToString();
}

void Grid::PrintAllCandidates() const {
    for (Index i = 0; i < sizeDim; ++i) {
        for (Index j = 0; j < sizeDim; ++j) {
            Value v = Get({i, j});
            for (Value val = 1; val <= sizeDim; ++val) {
                bool b = (v == 0 ? GetCandidates({i, j}).test(val) : false);
                std::cout << (b ? std::to_string(val) : std::string("."));
            }
            std::cout << " ";
        }
        std::cout << std::endl;
    }
}

/* unused */
void Grid::RestoreEliminate(Index index, Value value) {
    const auto& [row, col] = GetRowCol(index);
    assert(value > 0);

    for (Index i = 0; i < sizeDim; ++i) {
        if (Get({i, col}) > 0) continue;
        auto cands = GetCandidates({i, col});
        // assert(!cands->test(value));
        cands.set(value);
    }
    for (Index i = 0; i < sizeDim; ++i) {
        if (Get({row, i}) > 0) continue;
        auto cands = GetCandidates({row, i});
        // assert(!cands.test(value));
        cands.set(value);
    }
    Index boxRStart = (row / size) * size;
    Index boxCStart = (col / size) * size;
    for (Index i = boxRStart; i < boxRStart + size; ++i) {
        for (Index j = boxCStart; j < boxCStart + size; ++j) {
            if (Get({i, j}) > 0) continue;
            auto cands = GetCandidates({i, j});
            // assert(!cands.test(value));
            cands.set(value);
        }
    }
}
