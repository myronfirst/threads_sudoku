#include <cassert>
#include <chrono>
#include <iostream>
#include <string>

#include "Grid.h"
#include "Solver.h"
#include "Validator.h"

/***
 *  Pick Algorithm
***/
// constexpr auto SolveAlgorithm = Solver::ALGORITHM_LABEL::NAIVE;
// constexpr auto SolveAlgorithm = Solver::ALGORITHM_LABEL::CANDIDATE;
constexpr auto SolveAlgorithm = Solver::ALGORITHM_LABEL::THREAD_NAIVE;
// constexpr auto SolveAlgorithm = Solver::ALGORITHM_LABEL::THREAD_CANDIDATE;

/***
 *  Stop Solving after a solution is found (AKA find all possible solutions)
***/
// constexpr auto StopSolving = true;
constexpr auto StopSolving = false;

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "Check input" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::string inputFilename(argv[1]);
    std::string validSolutionsFilename(argv[2]);

    Grid input{inputFilename};
    assert(input.IsValid());
    // input.Print();
    // input.PrintAllCandidates();

    Solver solver{input, SolveAlgorithm, StopSolving};
    auto inputSolutions = solver.Solve();

    Validator validator{inputSolutions, validSolutionsFilename};
    bool isValid = validator.Validate();

    return (isValid ? 0 : -1);
}
