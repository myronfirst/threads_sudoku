#include "Solver.h"

#include <cassert>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <thread>

auto Solver::FindCandidate(const Grid& grid) -> std::tuple<Index, Value, FINDCANDIDATE_MSG> {
    Index minCount = std::numeric_limits<Index>::max();
    Index minIndex = std::numeric_limits<Index>::max();
    Value value = 0;
    for (Index index = 0; index < grid.sizeTotal; ++index) {
        if (grid.Get(index) > 0) continue;
        auto& cands = grid.GetCandidates(index);
        Value count = static_cast<Value>(cands.count());
        if (count == 0) return {grid.sizeTotal, 0, FINDCANDIDATE_MSG::FAILED};    //found cell with no candidates
        if (count < minCount) {
            minCount = count;
            minIndex = index;    // store min candidate index
            for (Value val = 1; val <= grid.sizeDim; ++val) {
                if (!(cands.test(val))) continue;
                value = val;    //store min candidate value
                break;
            }
        }
        if (count == 1) return {minIndex, value, FINDCANDIDATE_MSG::SINGLE};    //found cell with single candidate
    }
    if (minCount == std::numeric_limits<Index>::max()) return {grid.sizeTotal, 0, FINDCANDIDATE_MSG::SOLVED};    //sudoku is solved
    //else
    return {minIndex, value, FINDCANDIDATE_MSG::MULTIPLE};    //all cells contain at least 2 candidates
}

auto Solver::ApplyHeuristics(Grid& grid) -> std::tuple<Index, APPLYHEURISTICS_MSG> {
    while (true) {
        auto [minIndex, value, msg] = FindCandidate(grid);
        if (msg == FINDCANDIDATE_MSG::FAILED) return {std::numeric_limits<Index>::max(), APPLYHEURISTICS_MSG::FAILED};                  //found cell with no candidates
        if (msg == FINDCANDIDATE_MSG::SOLVED) return {std::numeric_limits<Index>::max(), APPLYHEURISTICS_MSG::SOLVED};                  //sudoku is solved
        if (msg == FINDCANDIDATE_MSG::MULTIPLE) return {minIndex, APPLYHEURISTICS_MSG::BACKTRACK};                                      //all cells contain at least 2 candidates, time to backtrack
        assert(msg == FINDCANDIDATE_MSG::SINGLE);                                                                                       //msg == 1 found cell with single candidate
        if (grid.EliminateAndPut(minIndex, value) == false) return {std::numeric_limits<Index>::max(), APPLYHEURISTICS_MSG::FAILED};    //found cell with invalid state after check
    }
    assert(false);
}

bool Solver::Backtrack(Grid& grid, Index minIndex) {
    Grid clone{grid};
    auto& cands = grid.GetCandidates(minIndex);
    assert(cands.count() > 1);
    for (Value val = 1; val <= grid.sizeDim; ++val) {
        if (!(cands.test(val))) continue;
        if (grid.EliminateAndPut(minIndex, val) == false) return false;    //found cell with invalid state after check
        if (CandidateBacktrackLinearSolveImpl(grid) == true) return true;
        //else a cell with no candidates has been found or checked
        grid = clone;    //also restores grid.emptyCells to previous state
    }
    return false;    //exhausted all candidates
}

bool Solver::CandidateBacktrackLinearSolveImpl(Grid& grid) {
    assert(grid.emptyCells <= grid.sizeTotal);
    /* Base Case */
    if (grid.emptyCells == 0) return StopSolving(grid);

    /* Apply Heuristics */
    auto [minIndex, msg] = ApplyHeuristics(grid);
    if (msg == APPLYHEURISTICS_MSG::SOLVED) return StopSolving(grid);
    if (msg == APPLYHEURISTICS_MSG::FAILED) return false;

    assert(msg == APPLYHEURISTICS_MSG::BACKTRACK);
    assert(minIndex < grid.sizeTotal);
    assert(grid.Get(minIndex) == 0);

    /* Backtracking Part */
    return Backtrack(grid, minIndex);
}

bool Solver::CandidateBacktrackLinearSolve(Grid& grid) {
    CandidateBacktrackLinearSolveImpl(grid);
    return solutions.size() > 0;
}

bool Solver::NaiveBacktrackLinearSolveImpl(Grid& grid, Grid::Index index) {
    if (index >= grid.sizeTotal) return StopSolving(grid);
    if (grid.Get(index) > 0) return NaiveBacktrackLinearSolveImpl(grid, index + 1);
    auto& cands = grid.GenerateCandidates(index);
    for (Value val = 1; val <= grid.sizeDim; ++val) {
        if (!(cands.test(val))) continue;
        grid.Put(index, val);
        if (NaiveBacktrackLinearSolveImpl(grid, index + 1)) return true;
        grid.Put(index, 0);
    }
    return false;
}

bool Solver::NaiveBacktrackLinearSolve(Grid& grid) {
    NaiveBacktrackLinearSolveImpl(grid, 0);
    return solutions.size() > 0;
}

namespace {
    constexpr auto ThreadInputGridsSize = 10;
    constexpr auto Threads = 1;
    constexpr auto UsingThreads = true;
}    // namespace

void Solver::ThreadLinearSolveImpl(Grid& grid) {
    std::stack<Grid> threadInputGrids = GenerateThreadInputGrids(grid, ThreadInputGridsSize);
    SpawnThreads(threadInputGrids, Threads);
}

bool Solver::ThreadLinearSolve(Grid& grid) {
    ThreadLinearSolveImpl(grid);
    return solutions.size() > 0;
}

auto Solver::GenerateThreadInputGrids(const Grid& grid, size_t maxSize) -> std::stack<Grid> {
    std::stack<Grid> stack{};
    for (const auto& index : grid.GetEmptyCells())
        for (const auto& val : grid.GetValidCandidates(index)) {
            Grid copy(grid);
            if (copy.EliminateAndPut(index, val) == false) continue;
            stack.emplace(copy);
            if (stack.size() == maxSize) return stack;
        }
    return stack;
}

void Solver::SpawnThreads(std::stack<Grid>& threadInputGrids, size_t size) {
    std::vector<std::thread> threads;
    for (size_t i = 0; i < size; ++i)
        threads.emplace_back([this](std::stack<Grid>& arg) { ThreadSolve(arg); }, std::ref(threadInputGrids));

    for (auto& t : threads) t.join();
}

void Solver::ThreadSolve(std::stack<Grid>& threadInputGrids) {
    while (!threadInputGrids.empty()) {
        auto optGrid = PopSafe(threadInputGrids);
        if (optGrid.has_value() == false) break;
        assert(threadAlgorithm);
        threadAlgorithm(optGrid.value());
    }
}

bool Solver::StopSolving(const Grid& grid) {
    if (UsingThreads)
        EmplaceSafe(solutions, grid);
    else
        solutions.emplace_back(grid);
    return stopSolving;
}

void Solver::EmplaceSafe(std::vector<Grid>& vec, const Grid& val) {
    const std::lock_guard<std::mutex> lock(emplaceMutex);
    vec.emplace_back(val);
}

auto Solver::PopSafe(std::stack<Grid>& stack) -> std::optional<Grid> {
    const std::lock_guard<std::mutex> lock(popMutex);
    if (stack.empty()) return {};
    assert(stack.empty() == false);
    auto top = stack.top();
    stack.pop();
    return top;
}

Solver::Solver(const Grid& _input, ALGORITHM_LABEL algorithmLabel, bool _stopSolving) : input{_input}, stopSolving{_stopSolving} {
    switch (algorithmLabel) {
        case ALGORITHM_LABEL::NAIVE: algorithm = [this](Grid& grid) -> bool { return NaiveBacktrackLinearSolve(grid); }; break;
        case ALGORITHM_LABEL::CANDIDATE: algorithm = [this](Grid& grid) -> bool { return CandidateBacktrackLinearSolve(grid); }; break;
        case ALGORITHM_LABEL::THREAD_NAIVE:
            algorithm = [this](Grid& grid) -> bool { return ThreadLinearSolve(grid); };
            threadAlgorithm = [this](Grid& grid) -> bool { return NaiveBacktrackLinearSolveImpl(grid, 0); };
            break;
        case ALGORITHM_LABEL::THREAD_CANDIDATE:
            algorithm = [this](Grid& grid) -> bool { return ThreadLinearSolve(grid); };
            threadAlgorithm = [this](Grid& grid) -> bool { return CandidateBacktrackLinearSolveImpl(grid); };
            break;
        case ALGORITHM_LABEL::ALGORITHM_MAX:
        default: assert(false); break;
    }
    assert(algorithm);
}

auto Solver::Solver::Solve() -> std::vector<GridVec> {
    Grid grid{input};
    bool solved{false};
    assert(algorithm);

    auto start = std::chrono::high_resolution_clock::now();
    solved = algorithm(grid);
    auto end = std::chrono::high_resolution_clock::now();

    AssertSolutions();
    PrintSolutions("out.txt");
    std::string str = solved ? "SOLVED" : "NOT SOLVED";
    std::cout << str << std::endl;

    std::chrono::duration<double> duration = end - start;
    std::cout << duration.count() << " sec\n";
    std::vector<GridVec> vec{};
    for (const auto& sol : solutions)
        vec.push_back(sol.grid);
    return vec;
}

void Solver::AssertSolutions() const {
    for (const auto& solution : solutions) {
        assert(solution.IsComplete());
        assert(solution.IsValid());
    }
}

void Solver::PrintSolutions(const std::string& filename) const {
    std::ofstream ofs;
    if (!filename.empty()) {
        ofs.open(filename, std::ios::out);
    }
    std::ostringstream oss{};
    for (const auto& sol : solutions) {
        oss << sol.ToString();
        oss << std::endl;
    }
    std::ostream& os = (ofs.is_open() ? ofs : std::cout);
    os << oss.str();
}
