#include "Validator.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>

namespace {
    using GridVec = std::vector<Grid::Value>;
    using MyVec = std::vector<GridVec>;
    void Sort(MyVec& vec) {
        std::sort(std::begin(vec), std::end(vec));
    }
    bool ContainsDuplicates(const MyVec& vec) {
        return (std::adjacent_find(std::cbegin(vec), std::cend(vec)) != std::cend(vec));
    }
    bool Subsequence(const MyVec& vec1, const MyVec& vec2) {
        return std::includes(std::cbegin(vec1), std::cend(vec1), std::cbegin(vec2), std::cend(vec2));
    }

}    // namespace

Validator::Validator(const std::vector<GridVec>& _inputSolutions, const std::string& fileName)
    : inputSolutions(_inputSolutions), validSolutions{} {
    unsigned long long sizeTotal{inputSolutions.front().size()};
    for (const auto& s : inputSolutions) assert(sizeTotal == s.size());
    std::ifstream ifs(fileName);
    if (!ifs.is_open()) {
        std::cout << "Check file path" << std::endl;
        exit(EXIT_FAILURE);
    }
    GridVec gridVec{};
    unsigned long long num;
    ifs >> num;
    while (ifs) {
        gridVec.emplace_back(static_cast<Value>(num));
        assert(gridVec.size() <= sizeTotal);
        if (gridVec.size() == sizeTotal) {
            validSolutions.emplace_back(gridVec);
            gridVec.clear();
        }
        ifs >> num;
    }
    if (gridVec.empty() == false) validSolutions.emplace_back(gridVec);
    inputSolutions.shrink_to_fit();
    validSolutions.shrink_to_fit();
}

bool Validator::Validate() {
    Sort(inputSolutions);
    Sort(validSolutions);
    assert(ContainsDuplicates(inputSolutions) == false);
    assert(ContainsDuplicates(validSolutions) == false);
    bool ret = Subsequence(validSolutions, inputSolutions);
    std::string str = ret ? "SUCCESS" : "FAIL";
    std::cout << "VALIDATOR " << str << std::endl;
    return ret;
}
